# api-gateway



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Run app on local environment

```
cd api-gateway
docker build -t api-gateway:latest -f DockerfileLocal .
docker images
docker run -p 8080:8080 api-gateway:latest
```

## Run with minikube method 1 from local project

```
cd api-gateway
docker build -t api-gateway:latest -f Dockerfile .
cd ../api-gateway-chart
docker images
minikube start
kubectl create namespace dev-namespace
minikube image load api-gateway:latest
minikube image ls --format table
helm install api-gateway-chart . --set image.repository=docker.io/library/api-gateway,image.tag=latest --namespace dev-namespace --values values-dev.yaml
# on lens set manualy variables on config/configmaps and restart the deployment
# on lens set manualy port 8080 on worklads/pods/ports/forward
```

## Run with minikube method 2 from container registry

```
cd ../api-gateway-chart
docker pull registry.gitlab.com/phou.jeannory/aws-1-api-gateway:latest
docker images
minikube start
kubectl create namespace dev-namespace
minikube image load registry.gitlab.com/phou.jeannory/aws-1-api-gateway:latest
helm install api-gateway-chart . --namespace dev-namespace --values values-dev.yaml
# on lens set manualy variables on config/configmaps and restart the deployment
# on lens set manualy port 8081 on worklads/pods/ports/forward
```

## Run with minikube method 3 from container registry

```
cd api-gateway-chart
minikube start
kubectl create namespace dev-namespace
helm install api-gateway-chart . --set image.repository=registry.gitlab.com/phou.jeannory/aws-1-api-gateway,image.tag=latest --namespace dev-namespace --values values-dev.yaml
# on lens set manualy variables on config/configmaps and restart the deployment
# on lens set manualy port 8081 on worklads/pods/ports/forward
# 2nd choice to port forward
minikube service --url api-gateway-container-service -n dev-namespace
```
