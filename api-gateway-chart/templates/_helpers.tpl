{{/* templates/_helpers.tpl */}}
{{- define "api-gateway-chart.fullname" -}}
{{- printf "%s" .Release.Name -}}
{{- end -}}

{{- define "api-gateway-chart.name" -}}
{{- printf "%s" .Chart.Name -}}
{{- end -}}

{{/* Définition du modèle server-port */}}
{{- define "server-port" -}}
{{ .Values.server.port }}
{{- end -}}
